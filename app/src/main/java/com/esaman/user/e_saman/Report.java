package com.esaman.user.e_saman;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import java.util.ArrayList;



public class Report extends AppCompatActivity {
    GridView gridView;
    ArrayList<Reported> list;
    ReportedListAdapter adapter = null;
    Student student=new Student();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        gridView=(GridView)findViewById(R.id.gridView);
        list=new ArrayList<>();
        adapter = new ReportedListAdapter(Report.this,R.layout.report_form_items,list);
        gridView.setAdapter(adapter);

       String StoredMatrix=student.storedMatrix;
        //getall data from sqlite
        Cursor cursor = Student.sqLiteHelper.getData("SELECT * FROM REPORTT where matrix='"+StoredMatrix+"'");
        list.clear();
        while(cursor.moveToNext()){
            int id = cursor.getInt(0);
            String depart= cursor.getString(1);
            String matrix= cursor.getString(2);
            String date= cursor.getString(3);
            String price= cursor.getString(4);
            String desc =cursor.getString(5);
            byte[] image=cursor.getBlob(6);

            list.add(new Reported(id,depart,matrix,date,price,desc,image));
        }
        adapter.notifyDataSetChanged();

    }


}
