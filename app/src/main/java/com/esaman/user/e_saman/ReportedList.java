package com.esaman.user.e_saman;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.GridView;

import java.util.ArrayList;


public class ReportedList extends AppCompatActivity {
    GridView gridView;
    ArrayList<Reported> list;
    ReportedListAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_form_list_activity);

        gridView=(GridView)findViewById(R.id.gridView);
        list=new ArrayList<>();
        adapter = new ReportedListAdapter(this,R.layout.report_form_items,list);
        gridView.setAdapter(adapter);

        //getall data from sqlite
        Cursor cursor = ReportFormStaff.sqLiteHelper.getData("SELECT * FROM REPORTT");
        list.clear();
        while(cursor.moveToNext()){
            int id = cursor.getInt(0);
            String depart= cursor.getString(1);
            String matrix= cursor.getString(2);
            String date= cursor.getString(3);
            String price= cursor.getString(4);
            String desc =cursor.getString(5);
            byte[] image=cursor.getBlob(6);

            list.add(new Reported(id,depart,matrix,date,price,desc,image));
        }
        adapter.notifyDataSetChanged();

    }


}
