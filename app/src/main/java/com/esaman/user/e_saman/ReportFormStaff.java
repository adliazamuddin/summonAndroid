package com.esaman.user.e_saman;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ReportFormStaff extends AppCompatActivity {

    EditText edtDepart,edtMatrix,edtDate,edtPrice,edtDesc;
    Button btnChoose,btnAdd,btnList,btnPic;;
    ImageView imageView;

    final int REQUEST_CODE_GALLERY=999;
    private static final int CAM_REQUEST=1313;

    public static SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_form_staff);


        init();

        sqLiteHelper=new SQLiteHelper(this, "report.sqlite",null,1);

        sqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS REPORTT (Id INTEGER PRIMARY KEY AUTOINCREMENT,depart VARCHAR,matrix VARCHAR,date VARCHAR,price VARCHAR,desc VARCHAR,image BLOG)");

        //listen to button choose to take photo from gallery
        btnChoose.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view)
            {
                ActivityCompat.requestPermissions(
                        ReportFormStaff.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });

        //set action to button add,to insert data into database
        btnAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                try{
                    sqLiteHelper.insertData(
                            edtDepart.getText().toString().trim(),
                            edtMatrix.getText().toString().trim(),
                            edtDate.getText().toString().trim(),
                            edtPrice.getText().toString().trim(),
                            edtDesc.getText().toString().trim(),
                            imageViewToByte(imageView)
                    );
                    Toast.makeText(getApplicationContext(),"Added Successfully !",Toast.LENGTH_LONG).show();
                    edtDepart.setText("");
                    edtMatrix.setText("");
                    edtDate.setText("");
                    edtPrice.setText("");
                    edtDesc.setText("");
                    imageView.setImageResource(R.mipmap.ic_launcher);
                }
                catch(Exception e){
                    e.printStackTrace();
                }

            }
        });

        btnList.setOnClickListener(new View.OnClickListener(){
            @Override
            public void  onClick(View view)
            {
                Intent intent=new Intent(ReportFormStaff.this,ReportedList.class);
                startActivity(intent);
            }
        });

        btnPic.setOnClickListener(new View.OnClickListener(){
            @Override
            public void  onClick(View view)
            {
                Intent intent =new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,CAM_REQUEST);
            }
        });
    }

    //return image to png
    private byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] byteArray=stream.toByteArray();
        return byteArray;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_CODE_GALLERY)
        {
            if(grantResults.length>0&&grantResults[0]== PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,REQUEST_CODE_GALLERY);
            }
            else{
                Toast.makeText(getApplicationContext(),"You dont have permission to access file location !",Toast.LENGTH_LONG).show();
            }
            return;
        }
        if(requestCode==CAM_REQUEST)
        {
            if(grantResults.length>0&&grantResults[0]== PackageManager.PERMISSION_GRANTED)
            {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,CAM_REQUEST);
            }
            else{
                Toast.makeText(getApplicationContext(),"You dont have permission to access file location !",Toast.LENGTH_LONG).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==REQUEST_CODE_GALLERY && resultCode==RESULT_OK && data != null)
        {
            Uri uri=data.getData();

            try{
                InputStream inputStream= getContentResolver().openInputStream(uri);

                Bitmap bitmap =BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);
            }catch(FileNotFoundException e){
                e.printStackTrace();
            }

        }
        else if(requestCode == CAM_REQUEST && resultCode==RESULT_OK && data != null){
                Bitmap bitmap=(Bitmap) data.getExtras().get("data");
                imageView.setImageBitmap(bitmap);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    private void init()
    {
        edtDepart=(EditText)findViewById(R.id.edtDepart);
        edtMatrix=(EditText)findViewById(R.id.edtMatrix);
        edtDate=(EditText)findViewById(R.id.edtDate);
        edtPrice=(EditText)findViewById(R.id.edtPrice);
        edtDesc=(EditText)findViewById(R.id.edtDesc);
        btnChoose=(Button)findViewById(R.id.btnChoose);
        btnAdd=(Button)findViewById(R.id.btnAdd);
        btnList=(Button)findViewById(R.id.btnList);
        imageView=(ImageView)findViewById(R.id.imageView);
        btnPic=(Button) findViewById(R.id.btnPic);

    }
}
