package com.esaman.user.e_saman;



public class Reported {
    private int id;
    private String depart;
    private String matrix;
    private String date;
    private String price;
    private String desc;
    private byte[] image;

    public Reported(int id, String depart, String date, String price, String matrix, String desc, byte[] image) {
        this.id = id;
        this.depart = depart;
        this.date = date;
        this.price = price;
        this.matrix = matrix;
        this.desc = desc;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getMatrix() {
        return matrix;
    }

    public void setMatrix(String matrix) {
        this.matrix = matrix;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
