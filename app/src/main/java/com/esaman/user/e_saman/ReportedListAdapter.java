package com.esaman.user.e_saman;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class ReportedListAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private ArrayList<Reported> reportsList;

    public ReportedListAdapter(Context context, int layout, ArrayList<Reported> reportsList) {
        this.context = context;
        this.layout = layout;
        this.reportsList = reportsList;
    }

    @Override
    public int getCount() {
        return reportsList.size();
    }

    @Override
    public Object getItem(int position) {
        return reportsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView txtDepart,txtMatrix,txtDate,txtPrice,txtDesc;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View row=view;
        ViewHolder holder=new ViewHolder();

        if(row == null)
        {
            LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout,null);

            holder.txtDepart=(TextView) row.findViewById(R.id.txtDepart);
            holder.txtMatrix=(TextView) row.findViewById(R.id.txtMatrix);
            holder.txtDate=(TextView) row.findViewById(R.id.txtDate);
            holder.txtPrice=(TextView) row.findViewById(R.id.txtPrice);
            holder.txtDesc=(TextView)row.findViewById(R.id.txtDesc);
            holder.imageView=(ImageView)row.findViewById(R.id.imgReport);
            row.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) row.getTag();
        }

        Reported report=reportsList.get(position);

        holder.txtDepart.setText(report.getDepart());
        holder.txtMatrix.setText(report.getMatrix());
        holder.txtDate.setText(report.getDate());
        holder.txtPrice.setText(report.getPrice());
        holder.txtDesc.setText(report.getDesc());

        byte[] reportImage=report.getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(reportImage,0,reportImage.length);
        holder.imageView.setImageBitmap(bitmap);


        return row;
    }


    public SQLiteDatabase db;
    // Database open/upgrade helper
    private DataBaseHelper dbHelper;
    public  ReportedListAdapter(Context _context)
    {
        context = _context;
        dbHelper = new DataBaseHelper(context, "report.sqlite",null,1);
    }
    public ReportedListAdapter open() throws SQLException
    {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close()
    {
        db.close();
    }
    public String getMatrixEntry(String matrix)
    {
        Cursor cursor=db.query("REPORTT", null, " matrix=?", new String[]{matrix}, null, null, null);
        if(cursor.getCount()<1)
        {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.close();
        return matrix;
    }

}
