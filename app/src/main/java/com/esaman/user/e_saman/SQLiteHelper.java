package com.esaman.user.e_saman;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

/**
 * Created by Eli on 25/2/2017.
 */

public class SQLiteHelper extends SQLiteOpenHelper {
    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    //for execute the database query
    public void queryData(String sql){
        SQLiteDatabase database=getWritableDatabase();
        database.execSQL(sql);
    }
    //for insert data into database
    public void insertData(String depart,String matrix,String date,String price,String desc,byte[] image){
        SQLiteDatabase database=getWritableDatabase();
        String sql = "INSERT INTO REPORTT VALUES (NULL,?,?,?,?,?,?)";

        SQLiteStatement statement= database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1,depart);
        statement.bindString(2,matrix);
        statement.bindString(3,date);
        statement.bindString(4,price);
        statement.bindString(5,desc);
        statement.bindBlob(6,image);

        statement.executeInsert();
    }

    //to show the data inside the database
    public Cursor getData(String sql)
    {
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql,null);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
