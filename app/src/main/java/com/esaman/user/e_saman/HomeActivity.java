package com.esaman.user.e_saman;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
    public void showClick(View view)
    {
        String button_text;
        button_text=((Button)view).getText().toString();
        if(button_text.equals("Staff"))
        {
            Intent intent=new Intent(this,Staff.class);
            startActivity(intent);
        }
        else if(button_text.equals("Student"))
        {
            Intent intent=new Intent(this,Student.class);
            startActivity(intent);
        }
    }
}
