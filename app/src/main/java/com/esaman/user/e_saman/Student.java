package com.esaman.user.e_saman;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Student extends AppCompatActivity {

    Button buttonStudLogin;
    ReportedListAdapter reportedList;
    public static SQLiteHelper sqLiteHelper;
    public static String storedMatrix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        sqLiteHelper=new SQLiteHelper(this, "report.sqlite",null,1);

        reportedList=new ReportedListAdapter(this);
        reportedList=reportedList.open();

        buttonStudLogin=(Button)findViewById(R.id.buttonStudLogin);

    }
    public void showClickStudent(View view)
    {
        String button_text;
        button_text=((Button)view).getText().toString();
        if(button_text.equals("Back"))
        {
            Intent intent=new Intent(this,HomeActivity.class);
            startActivity(intent);
        }
    }


    public void signInStud(View V)
    {

        // get the Refferences of views
        final EditText edtMatrixToLogin=(EditText)findViewById(R.id.edtMatrixToLogin);

        Button buttonStudLogin=(Button)findViewById(R.id.buttonStudLogin);

        // Set On ClickListener
        buttonStudLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // get The User name and Password
                String matrix=edtMatrixToLogin.getText().toString();

                // fetch the Password form database for respective user name
                storedMatrix=reportedList.getMatrixEntry(matrix);


                // check if the Stored password matches with  Password entered by user
                if(matrix.equals(storedMatrix))
                {
                    Toast.makeText(Student.this, "You are Reported", Toast.LENGTH_LONG).show();
                    Intent intent=new Intent(Student.this,Report.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(Student.this, "No Report", Toast.LENGTH_LONG).show();
                }
            }
        });



    }
}
